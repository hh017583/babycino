#include <stdio.h>
#include <stdlib.h>

union ilword {
    int n;
    union ilword* ptr;
    void(*f)();
};
typedef union ilword word;

word param[35];
int next_param = 0;

word r0 = {0};

word vg0 = {0};
word vg1 = {0};
word vg2 = {0};
word vg3 = {0};
word vg4 = {0};
word vg5 = {0};
void INIT();
void MAIN();
void TV_Start();
void Tree_Init();
void Tree_SetRight();
void Tree_SetLeft();
void Tree_GetRight();
void Tree_GetLeft();
void Tree_GetKey();
void Tree_SetKey();
void Tree_GetHas__Right();
void Tree_GetHas__Left();
void Tree_SetHas__Left();
void Tree_SetHas__Right();
void Tree_Compare();
void Tree_Insert();
void Tree_Delete();
void Tree_Remove();
void Tree_RemoveRight();
void Tree_RemoveLeft();
void Tree_Search();
void Tree_Print();
void Tree_RecPrint();
void Tree_accept();
void Visitor_visit();
void MyVisitor_visit();
int main() {
    INIT();
    MAIN();
    return 0;
}

void INIT() {
    word vl[0];
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
INIT:
    r1.n = 1;
    r2.n = 0;
    vg0.ptr = calloc(r2.n, sizeof(word));
    r2.n = 0;
    vg1.ptr = calloc(r2.n, sizeof(word));
    r2.n = 1;
    vg2.ptr = calloc(r2.n, sizeof(word));
    r3 = vg2;
    r4.f = &TV_Start;
    *(r3.ptr) = r4;
    r2.n = 21;
    vg3.ptr = calloc(r2.n, sizeof(word));
    r3 = vg3;
    r4.f = &Tree_Init;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_SetRight;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_SetLeft;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_GetRight;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_GetLeft;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_GetKey;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_SetKey;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_GetHas__Right;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_GetHas__Left;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_SetHas__Left;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_SetHas__Right;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Compare;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Insert;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Delete;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Remove;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_RemoveRight;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_RemoveLeft;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Search;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_Print;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_RecPrint;
    *(r3.ptr) = r4;
    r3.ptr = r3.ptr + r1.n;
    r4.f = &Tree_accept;
    *(r3.ptr) = r4;
    r2.n = 1;
    vg4.ptr = calloc(r2.n, sizeof(word));
    r3 = vg4;
    r4.f = &Visitor_visit;
    *(r3.ptr) = r4;
    r2.n = 1;
    vg5.ptr = calloc(r2.n, sizeof(word));
    r3 = vg5;
    r4.f = &MyVisitor_visit;
    *(r3.ptr) = r4;
    return;
}

void MAIN() {
    word vl[0];
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= -1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
MAIN:
    r1.n = 1;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg2;
    r3 = *(r2.ptr);
    r4.n = 0;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = r2;
    (*(r6.f))();
    r7 = r0;
    printf("%d\n", r7);
    return;
}

void TV_Start() {
    word vl[5] = {0,0,0,0,0};
    word r118 = {0};
    word r117 = {0};
    word r116 = {0};
    word r115 = {0};
    word r114 = {0};
    word r113 = {0};
    word r112 = {0};
    word r111 = {0};
    word r110 = {0};
    word r109 = {0};
    word r108 = {0};
    word r107 = {0};
    word r106 = {0};
    word r105 = {0};
    word r104 = {0};
    word r103 = {0};
    word r102 = {0};
    word r101 = {0};
    word r100 = {0};
    word r99 = {0};
    word r98 = {0};
    word r97 = {0};
    word r96 = {0};
    word r95 = {0};
    word r94 = {0};
    word r93 = {0};
    word r92 = {0};
    word r91 = {0};
    word r90 = {0};
    word r89 = {0};
    word r88 = {0};
    word r87 = {0};
    word r86 = {0};
    word r85 = {0};
    word r84 = {0};
    word r83 = {0};
    word r82 = {0};
    word r81 = {0};
    word r80 = {0};
    word r79 = {0};
    word r78 = {0};
    word r77 = {0};
    word r76 = {0};
    word r75 = {0};
    word r74 = {0};
    word r73 = {0};
    word r72 = {0};
    word r71 = {0};
    word r70 = {0};
    word r69 = {0};
    word r68 = {0};
    word r67 = {0};
    word r66 = {0};
    word r65 = {0};
    word r64 = {0};
    word r63 = {0};
    word r62 = {0};
    word r61 = {0};
    word r60 = {0};
    word r59 = {0};
    word r58 = {0};
    word r57 = {0};
    word r56 = {0};
    word r55 = {0};
    word r54 = {0};
    word r53 = {0};
    word r52 = {0};
    word r51 = {0};
    word r50 = {0};
    word r49 = {0};
    word r48 = {0};
    word r47 = {0};
    word r46 = {0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 4 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
TV_Start:
    r1.n = 7;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg3;
    vl[1] = r2;
    r3.n = 16;
    r4 = *(vl[1].ptr);
    r5.n = 0;
    r6.ptr = r4.ptr + r5.n;
    r7 = *(r6.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r3;
    (*(r7.f))();
    r9 = *(vl[1].ptr);
    r10.n = 18;
    r11.ptr = r9.ptr + r10.n;
    r12 = *(r11.ptr);
    param[next_param++] = vl[1];
    (*(r12.f))();
    r14.n = 100000000;
    printf("%d\n", r14);
    r15.n = 8;
    r16 = *(vl[1].ptr);
    r17.n = 12;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r15;
    (*(r19.f))();
    r21.n = 24;
    r22 = *(vl[1].ptr);
    r23.n = 12;
    r24.ptr = r22.ptr + r23.n;
    r25 = *(r24.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r21;
    (*(r25.f))();
    r27.n = 4;
    r28 = *(vl[1].ptr);
    r29.n = 12;
    r30.ptr = r28.ptr + r29.n;
    r31 = *(r30.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r27;
    (*(r31.f))();
    r33.n = 12;
    r34 = *(vl[1].ptr);
    r35.n = 12;
    r36.ptr = r34.ptr + r35.n;
    r37 = *(r36.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r33;
    (*(r37.f))();
    r39.n = 20;
    r40 = *(vl[1].ptr);
    r41.n = 12;
    r42.ptr = r40.ptr + r41.n;
    r43 = *(r42.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r39;
    (*(r43.f))();
    r45.n = 28;
    r46 = *(vl[1].ptr);
    r47.n = 12;
    r48.ptr = r46.ptr + r47.n;
    r49 = *(r48.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r45;
    (*(r49.f))();
    r51.n = 14;
    r52 = *(vl[1].ptr);
    r53.n = 12;
    r54.ptr = r52.ptr + r53.n;
    r55 = *(r54.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r51;
    (*(r55.f))();
    r57 = *(vl[1].ptr);
    r58.n = 18;
    r59.ptr = r57.ptr + r58.n;
    r60 = *(r59.ptr);
    param[next_param++] = vl[1];
    (*(r60.f))();
    r62.n = 100000000;
    printf("%d\n", r62);
    r63.n = 3;
    r64.ptr = calloc(r63.n, sizeof(word));
    *(r64.ptr) = vg5;
    vl[4] = r64;
    r65.n = 50000000;
    printf("%d\n", r65);
    r66 = *(vl[1].ptr);
    r67.n = 20;
    r68.ptr = r66.ptr + r67.n;
    r69 = *(r68.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = vl[4];
    (*(r69.f))();
    r71.n = 100000000;
    printf("%d\n", r71);
    r72.n = 24;
    r73 = *(vl[1].ptr);
    r74.n = 17;
    r75.ptr = r73.ptr + r74.n;
    r76 = *(r75.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r72;
    (*(r76.f))();
    r77 = r0;
    printf("%d\n", r77);
    r78.n = 12;
    r79 = *(vl[1].ptr);
    r80.n = 17;
    r81.ptr = r79.ptr + r80.n;
    r82 = *(r81.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r78;
    (*(r82.f))();
    r83 = r0;
    printf("%d\n", r83);
    r84.n = 16;
    r85 = *(vl[1].ptr);
    r86.n = 17;
    r87.ptr = r85.ptr + r86.n;
    r88 = *(r87.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r84;
    (*(r88.f))();
    r89 = r0;
    printf("%d\n", r89);
    r90.n = 50;
    r91 = *(vl[1].ptr);
    r92.n = 17;
    r93.ptr = r91.ptr + r92.n;
    r94 = *(r93.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r90;
    (*(r94.f))();
    r95 = r0;
    printf("%d\n", r95);
    r96.n = 12;
    r97 = *(vl[1].ptr);
    r98.n = 17;
    r99.ptr = r97.ptr + r98.n;
    r100 = *(r99.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r96;
    (*(r100.f))();
    r101 = r0;
    printf("%d\n", r101);
    r102.n = 12;
    r103 = *(vl[1].ptr);
    r104.n = 13;
    r105.ptr = r103.ptr + r104.n;
    r106 = *(r105.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r102;
    (*(r106.f))();
    r108 = *(vl[1].ptr);
    r109.n = 18;
    r110.ptr = r108.ptr + r109.n;
    r111 = *(r110.ptr);
    param[next_param++] = vl[1];
    (*(r111.f))();
    r113.n = 12;
    r114 = *(vl[1].ptr);
    r115.n = 17;
    r116.ptr = r114.ptr + r115.n;
    r117 = *(r116.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r113;
    (*(r117.f))();
    r118 = r0;
    printf("%d\n", r118);
    r0.n = 0;
    return;
}

void Tree_Init() {
    word vl[2] = {0,0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Init:
    r2.n = 3;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r3.n = 0;
    r5.n = 4;
    r4.ptr = vl[0].ptr + r5.n;
    *(r4.ptr) = r3;
    r6.n = 0;
    r8.n = 5;
    r7.ptr = vl[0].ptr + r8.n;
    *(r7.ptr) = r6;
    r0.n = 1;
    return;
}

void Tree_SetRight() {
    word vl[2] = {0,0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_SetRight:
    r2.n = 2;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r0.n = 1;
    return;
}

void Tree_SetLeft() {
    word vl[2] = {0,0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_SetLeft:
    r2.n = 1;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r0.n = 1;
    return;
}

void Tree_GetRight() {
    word vl[1] = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 0 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_GetRight:
    r1.n = 2;
    r2.ptr = vl[0].ptr + r1.n;
    r3 = *(r2.ptr);
    r0 = r3;
    return;
}

void Tree_GetLeft() {
    word vl[1] = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 0 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_GetLeft:
    r1.n = 1;
    r2.ptr = vl[0].ptr + r1.n;
    r3 = *(r2.ptr);
    r0 = r3;
    return;
}

void Tree_GetKey() {
    word vl[1] = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 0 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_GetKey:
    r1.n = 3;
    r2.ptr = vl[0].ptr + r1.n;
    r3 = *(r2.ptr);
    r0 = r3;
    return;
}

void Tree_SetKey() {
    word vl[2] = {0,0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_SetKey:
    r2.n = 3;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r0.n = 1;
    return;
}

void Tree_GetHas__Right() {
    word vl[1] = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 0 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_GetHas__Right:
    r1.n = 5;
    r2.ptr = vl[0].ptr + r1.n;
    r3 = *(r2.ptr);
    r0 = r3;
    return;
}

void Tree_GetHas__Left() {
    word vl[1] = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 0 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_GetHas__Left:
    r1.n = 4;
    r2.ptr = vl[0].ptr + r1.n;
    r3 = *(r2.ptr);
    r0 = r3;
    return;
}

void Tree_SetHas__Left() {
    word vl[2] = {0,0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_SetHas__Left:
    r2.n = 4;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r0.n = 1;
    return;
}

void Tree_SetHas__Right() {
    word vl[2] = {0,0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_SetHas__Right:
    r2.n = 5;
    r1.ptr = vl[0].ptr + r2.n;
    *(r1.ptr) = vl[1];
    r0.n = 1;
    return;
}

void Tree_Compare() {
    word vl[5] = {0,0,0,0,0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 4 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Compare:
    r2.n = 1;
    r3.n = vl[2].n + r2.n;
    vl[4] = r3;
    r4.n = vl[1].n < vl[2].n;
    if (r4.n == 0) goto Tree_Compare_0;
    vl[3].n = 0;
    goto Tree_Compare_5;
Tree_Compare_0:
    r6.n = vl[1].n < vl[4].n;
    if (r6.n == 0) goto Tree_Compare_1;
    r7.n = 0;
    goto Tree_Compare_2;
Tree_Compare_1:
    r7.n = 1;
Tree_Compare_2:
    if (r7.n == 0) goto Tree_Compare_3;
    vl[3].n = 0;
    goto Tree_Compare_4;
Tree_Compare_3:
    vl[3].n = 1;
Tree_Compare_4:
Tree_Compare_5:
    r0 = vl[3];
    return;
}

void Tree_Insert() {
    word vl[7] = {0,0,0,0,0,0,0};
    word r57 = {0};
    word r56 = {0};
    word r55 = {0};
    word r54 = {0};
    word r53 = {0};
    word r52 = {0};
    word r51 = {0};
    word r50 = {0};
    word r49 = {0};
    word r48 = {0};
    word r47 = {0};
    word r46 = {0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 6 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Insert:
    r1.n = 7;
    r2.ptr = calloc(r1.n, sizeof(word));
    *(r2.ptr) = vg3;
    vl[2] = r2;
    r3 = *(vl[2].ptr);
    r4.n = 0;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = vl[2];
    param[next_param++] = vl[1];
    (*(r6.f))();
    vl[4] = vl[0];
    vl[5].n = 1;
Tree_Insert_6:
    if (vl[5].n == 0) goto Tree_Insert_13;
    r9 = *(vl[4].ptr);
    r10.n = 5;
    r11.ptr = r9.ptr + r10.n;
    r12 = *(r11.ptr);
    param[next_param++] = vl[4];
    (*(r12.f))();
    r13 = r0;
    vl[6] = r13;
    r14.n = vl[1].n < vl[6].n;
    if (r14.n == 0) goto Tree_Insert_9;
    r15 = *(vl[4].ptr);
    r16.n = 8;
    r17.ptr = r15.ptr + r16.n;
    r18 = *(r17.ptr);
    param[next_param++] = vl[4];
    (*(r18.f))();
    r19 = r0;
    if (r19.n == 0) goto Tree_Insert_7;
    r20 = *(vl[4].ptr);
    r21.n = 4;
    r22.ptr = r20.ptr + r21.n;
    r23 = *(r22.ptr);
    param[next_param++] = vl[4];
    (*(r23.f))();
    r24 = r0;
    vl[4] = r24;
    goto Tree_Insert_8;
Tree_Insert_7:
    vl[5].n = 0;
    r26.n = 1;
    r27 = *(vl[4].ptr);
    r28.n = 9;
    r29.ptr = r27.ptr + r28.n;
    r30 = *(r29.ptr);
    param[next_param++] = vl[4];
    param[next_param++] = r26;
    (*(r30.f))();
    r32 = *(vl[4].ptr);
    r33.n = 2;
    r34.ptr = r32.ptr + r33.n;
    r35 = *(r34.ptr);
    param[next_param++] = vl[4];
    param[next_param++] = vl[2];
    (*(r35.f))();
Tree_Insert_8:
    goto Tree_Insert_12;
Tree_Insert_9:
    r37 = *(vl[4].ptr);
    r38.n = 7;
    r39.ptr = r37.ptr + r38.n;
    r40 = *(r39.ptr);
    param[next_param++] = vl[4];
    (*(r40.f))();
    r41 = r0;
    if (r41.n == 0) goto Tree_Insert_10;
    r42 = *(vl[4].ptr);
    r43.n = 3;
    r44.ptr = r42.ptr + r43.n;
    r45 = *(r44.ptr);
    param[next_param++] = vl[4];
    (*(r45.f))();
    r46 = r0;
    vl[4] = r46;
    goto Tree_Insert_11;
Tree_Insert_10:
    vl[5].n = 0;
    r48.n = 1;
    r49 = *(vl[4].ptr);
    r50.n = 10;
    r51.ptr = r49.ptr + r50.n;
    r52 = *(r51.ptr);
    param[next_param++] = vl[4];
    param[next_param++] = r48;
    (*(r52.f))();
    r54 = *(vl[4].ptr);
    r55.n = 1;
    r56.ptr = r54.ptr + r55.n;
    r57 = *(r56.ptr);
    param[next_param++] = vl[4];
    param[next_param++] = vl[2];
    (*(r57.f))();
Tree_Insert_11:
Tree_Insert_12:
    goto Tree_Insert_6;
Tree_Insert_13:
    r0.n = 1;
    return;
}

void Tree_Delete() {
    word vl[9] = {0,0,0,0,0,0,0,0,0};
    word r55 = {0};
    word r54 = {0};
    word r53 = {0};
    word r52 = {0};
    word r51 = {0};
    word r50 = {0};
    word r49 = {0};
    word r48 = {0};
    word r47 = {0};
    word r46 = {0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 8 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Delete:
    vl[2] = vl[0];
    vl[3] = vl[0];
    vl[4].n = 1;
    vl[5].n = 0;
    vl[7].n = 1;
Tree_Delete_14:
    if (vl[4].n == 0) goto Tree_Delete_32;
    r4 = *(vl[2].ptr);
    r5.n = 5;
    r6.ptr = r4.ptr + r5.n;
    r7 = *(r6.ptr);
    param[next_param++] = vl[2];
    (*(r7.f))();
    r8 = r0;
    vl[8] = r8;
    r9.n = vl[1].n < vl[8].n;
    if (r9.n == 0) goto Tree_Delete_17;
    r10 = *(vl[2].ptr);
    r11.n = 8;
    r12.ptr = r10.ptr + r11.n;
    r13 = *(r12.ptr);
    param[next_param++] = vl[2];
    (*(r13.f))();
    r14 = r0;
    if (r14.n == 0) goto Tree_Delete_15;
    vl[3] = vl[2];
    r15 = *(vl[2].ptr);
    r16.n = 4;
    r17.ptr = r15.ptr + r16.n;
    r18 = *(r17.ptr);
    param[next_param++] = vl[2];
    (*(r18.f))();
    r19 = r0;
    vl[2] = r19;
    goto Tree_Delete_16;
Tree_Delete_15:
    vl[4].n = 0;
Tree_Delete_16:
    goto Tree_Delete_31;
Tree_Delete_17:
    r21.n = vl[8].n < vl[1].n;
    if (r21.n == 0) goto Tree_Delete_20;
    r22 = *(vl[2].ptr);
    r23.n = 7;
    r24.ptr = r22.ptr + r23.n;
    r25 = *(r24.ptr);
    param[next_param++] = vl[2];
    (*(r25.f))();
    r26 = r0;
    if (r26.n == 0) goto Tree_Delete_18;
    vl[3] = vl[2];
    r27 = *(vl[2].ptr);
    r28.n = 3;
    r29.ptr = r27.ptr + r28.n;
    r30 = *(r29.ptr);
    param[next_param++] = vl[2];
    (*(r30.f))();
    r31 = r0;
    vl[2] = r31;
    goto Tree_Delete_19;
Tree_Delete_18:
    vl[4].n = 0;
Tree_Delete_19:
    goto Tree_Delete_30;
Tree_Delete_20:
    if (vl[7].n == 0) goto Tree_Delete_28;
    r33 = *(vl[2].ptr);
    r34.n = 7;
    r35.ptr = r33.ptr + r34.n;
    r36 = *(r35.ptr);
    param[next_param++] = vl[2];
    (*(r36.f))();
    r37 = r0;
    if (r37.n == 0) goto Tree_Delete_21;
    r38.n = 0;
    goto Tree_Delete_22;
Tree_Delete_21:
    r38.n = 1;
Tree_Delete_22:
    r45 = r38;
    if (r45.n == 0) goto Tree_Delete_25;
    r39 = *(vl[2].ptr);
    r40.n = 8;
    r41.ptr = r39.ptr + r40.n;
    r42 = *(r41.ptr);
    param[next_param++] = vl[2];
    (*(r42.f))();
    r43 = r0;
    if (r43.n == 0) goto Tree_Delete_23;
    r44.n = 0;
    goto Tree_Delete_24;
Tree_Delete_23:
    r44.n = 1;
Tree_Delete_24:
    r45 = r44;
Tree_Delete_25:
    if (r45.n == 0) goto Tree_Delete_26;
    goto Tree_Delete_27;
Tree_Delete_26:
    r47 = *(vl[0].ptr);
    r48.n = 14;
    r49.ptr = r47.ptr + r48.n;
    r50 = *(r49.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[3];
    param[next_param++] = vl[2];
    (*(r50.f))();
Tree_Delete_27:
    goto Tree_Delete_29;
Tree_Delete_28:
    r52 = *(vl[0].ptr);
    r53.n = 14;
    r54.ptr = r52.ptr + r53.n;
    r55 = *(r54.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[3];
    param[next_param++] = vl[2];
    (*(r55.f))();
Tree_Delete_29:
    vl[5].n = 1;
    vl[4].n = 0;
Tree_Delete_30:
Tree_Delete_31:
    vl[7].n = 0;
    goto Tree_Delete_14;
Tree_Delete_32:
    r0 = vl[5];
    return;
}

void Tree_Remove() {
    word vl[6] = {0,0,0,0,0,0};
    word r67 = {0};
    word r66 = {0};
    word r65 = {0};
    word r64 = {0};
    word r63 = {0};
    word r62 = {0};
    word r61 = {0};
    word r60 = {0};
    word r59 = {0};
    word r58 = {0};
    word r57 = {0};
    word r56 = {0};
    word r55 = {0};
    word r54 = {0};
    word r53 = {0};
    word r52 = {0};
    word r51 = {0};
    word r50 = {0};
    word r49 = {0};
    word r48 = {0};
    word r47 = {0};
    word r46 = {0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 5 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Remove:
    r1 = *(vl[2].ptr);
    r2.n = 8;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[2];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto Tree_Remove_33;
    r6 = *(vl[0].ptr);
    r7.n = 16;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    param[next_param++] = vl[2];
    (*(r9.f))();
    goto Tree_Remove_38;
Tree_Remove_33:
    r11 = *(vl[2].ptr);
    r12.n = 7;
    r13.ptr = r11.ptr + r12.n;
    r14 = *(r13.ptr);
    param[next_param++] = vl[2];
    (*(r14.f))();
    r15 = r0;
    if (r15.n == 0) goto Tree_Remove_34;
    r16 = *(vl[0].ptr);
    r17.n = 15;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[1];
    param[next_param++] = vl[2];
    (*(r19.f))();
    goto Tree_Remove_37;
Tree_Remove_34:
    r21 = *(vl[2].ptr);
    r22.n = 5;
    r23.ptr = r21.ptr + r22.n;
    r24 = *(r23.ptr);
    param[next_param++] = vl[2];
    (*(r24.f))();
    r25 = r0;
    vl[4] = r25;
    r26 = *(vl[1].ptr);
    r27.n = 4;
    r28.ptr = r26.ptr + r27.n;
    r29 = *(r28.ptr);
    param[next_param++] = vl[1];
    (*(r29.f))();
    r30 = r0;
    r31 = *(r30.ptr);
    r32.n = 5;
    r33.ptr = r31.ptr + r32.n;
    r34 = *(r33.ptr);
    param[next_param++] = r30;
    (*(r34.f))();
    r35 = r0;
    vl[5] = r35;
    r36 = *(vl[0].ptr);
    r37.n = 11;
    r38.ptr = r36.ptr + r37.n;
    r39 = *(r38.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[4];
    param[next_param++] = vl[5];
    (*(r39.f))();
    r40 = r0;
    if (r40.n == 0) goto Tree_Remove_35;
    r41.n = 6;
    r42.ptr = vl[0].ptr + r41.n;
    r43 = *(r42.ptr);
    r44 = *(vl[1].ptr);
    r45.n = 2;
    r46.ptr = r44.ptr + r45.n;
    r47 = *(r46.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r43;
    (*(r47.f))();
    r49.n = 0;
    r50 = *(vl[1].ptr);
    r51.n = 9;
    r52.ptr = r50.ptr + r51.n;
    r53 = *(r52.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r49;
    (*(r53.f))();
    goto Tree_Remove_36;
Tree_Remove_35:
    r55.n = 6;
    r56.ptr = vl[0].ptr + r55.n;
    r57 = *(r56.ptr);
    r58 = *(vl[1].ptr);
    r59.n = 1;
    r60.ptr = r58.ptr + r59.n;
    r61 = *(r60.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r57;
    (*(r61.f))();
    r63.n = 0;
    r64 = *(vl[1].ptr);
    r65.n = 10;
    r66.ptr = r64.ptr + r65.n;
    r67 = *(r66.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r63;
    (*(r67.f))();
Tree_Remove_36:
Tree_Remove_37:
Tree_Remove_38:
    r0.n = 1;
    return;
}

void Tree_RemoveRight() {
    word vl[3] = {0,0,0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 2 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_RemoveRight:
Tree_RemoveRight_39:
    r1 = *(vl[2].ptr);
    r2.n = 7;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[2];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto Tree_RemoveRight_40;
    r6 = *(vl[2].ptr);
    r7.n = 3;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[2];
    (*(r9.f))();
    r10 = r0;
    r11 = *(r10.ptr);
    r12.n = 5;
    r13.ptr = r11.ptr + r12.n;
    r14 = *(r13.ptr);
    param[next_param++] = r10;
    (*(r14.f))();
    r15 = r0;
    r16 = *(vl[2].ptr);
    r17.n = 6;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = vl[2];
    param[next_param++] = r15;
    (*(r19.f))();
    vl[1] = vl[2];
    r21 = *(vl[2].ptr);
    r22.n = 3;
    r23.ptr = r21.ptr + r22.n;
    r24 = *(r23.ptr);
    param[next_param++] = vl[2];
    (*(r24.f))();
    r25 = r0;
    vl[2] = r25;
    goto Tree_RemoveRight_39;
Tree_RemoveRight_40:
    r26.n = 6;
    r27.ptr = vl[0].ptr + r26.n;
    r28 = *(r27.ptr);
    r29 = *(vl[1].ptr);
    r30.n = 1;
    r31.ptr = r29.ptr + r30.n;
    r32 = *(r31.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r28;
    (*(r32.f))();
    r34.n = 0;
    r35 = *(vl[1].ptr);
    r36.n = 10;
    r37.ptr = r35.ptr + r36.n;
    r38 = *(r37.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r34;
    (*(r38.f))();
    r0.n = 1;
    return;
}

void Tree_RemoveLeft() {
    word vl[3] = {0,0,0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 2 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_RemoveLeft:
Tree_RemoveLeft_41:
    r1 = *(vl[2].ptr);
    r2.n = 8;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[2];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto Tree_RemoveLeft_42;
    r6 = *(vl[2].ptr);
    r7.n = 4;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[2];
    (*(r9.f))();
    r10 = r0;
    r11 = *(r10.ptr);
    r12.n = 5;
    r13.ptr = r11.ptr + r12.n;
    r14 = *(r13.ptr);
    param[next_param++] = r10;
    (*(r14.f))();
    r15 = r0;
    r16 = *(vl[2].ptr);
    r17.n = 6;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = vl[2];
    param[next_param++] = r15;
    (*(r19.f))();
    vl[1] = vl[2];
    r21 = *(vl[2].ptr);
    r22.n = 4;
    r23.ptr = r21.ptr + r22.n;
    r24 = *(r23.ptr);
    param[next_param++] = vl[2];
    (*(r24.f))();
    r25 = r0;
    vl[2] = r25;
    goto Tree_RemoveLeft_41;
Tree_RemoveLeft_42:
    r26.n = 6;
    r27.ptr = vl[0].ptr + r26.n;
    r28 = *(r27.ptr);
    r29 = *(vl[1].ptr);
    r30.n = 2;
    r31.ptr = r29.ptr + r30.n;
    r32 = *(r31.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r28;
    (*(r32.f))();
    r34.n = 0;
    r35 = *(vl[1].ptr);
    r36.n = 9;
    r37.ptr = r35.ptr + r36.n;
    r38 = *(r37.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = r34;
    (*(r38.f))();
    r0.n = 1;
    return;
}

void Tree_Search() {
    word vl[6] = {0,0,0,0,0,0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 5 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Search:
    vl[2] = vl[0];
    vl[4].n = 1;
    vl[3].n = 0;
Tree_Search_43:
    if (vl[4].n == 0) goto Tree_Search_52;
    r3 = *(vl[2].ptr);
    r4.n = 5;
    r5.ptr = r3.ptr + r4.n;
    r6 = *(r5.ptr);
    param[next_param++] = vl[2];
    (*(r6.f))();
    r7 = r0;
    vl[5] = r7;
    r8.n = vl[1].n < vl[5].n;
    if (r8.n == 0) goto Tree_Search_46;
    r9 = *(vl[2].ptr);
    r10.n = 8;
    r11.ptr = r9.ptr + r10.n;
    r12 = *(r11.ptr);
    param[next_param++] = vl[2];
    (*(r12.f))();
    r13 = r0;
    if (r13.n == 0) goto Tree_Search_44;
    r14 = *(vl[2].ptr);
    r15.n = 4;
    r16.ptr = r14.ptr + r15.n;
    r17 = *(r16.ptr);
    param[next_param++] = vl[2];
    (*(r17.f))();
    r18 = r0;
    vl[2] = r18;
    goto Tree_Search_45;
Tree_Search_44:
    vl[4].n = 0;
Tree_Search_45:
    goto Tree_Search_51;
Tree_Search_46:
    r20.n = vl[5].n < vl[1].n;
    if (r20.n == 0) goto Tree_Search_49;
    r21 = *(vl[2].ptr);
    r22.n = 7;
    r23.ptr = r21.ptr + r22.n;
    r24 = *(r23.ptr);
    param[next_param++] = vl[2];
    (*(r24.f))();
    r25 = r0;
    if (r25.n == 0) goto Tree_Search_47;
    r26 = *(vl[2].ptr);
    r27.n = 3;
    r28.ptr = r26.ptr + r27.n;
    r29 = *(r28.ptr);
    param[next_param++] = vl[2];
    (*(r29.f))();
    r30 = r0;
    vl[2] = r30;
    goto Tree_Search_48;
Tree_Search_47:
    vl[4].n = 0;
Tree_Search_48:
    goto Tree_Search_50;
Tree_Search_49:
    vl[3].n = 1;
    vl[4].n = 0;
Tree_Search_50:
Tree_Search_51:
    goto Tree_Search_43;
Tree_Search_52:
    r0 = vl[3];
    return;
}

void Tree_Print() {
    word vl[3] = {0,0,0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 2 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_Print:
    vl[2] = vl[0];
    r1 = *(vl[0].ptr);
    r2.n = 19;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = vl[2];
    (*(r4.f))();
    r0.n = 1;
    return;
}

void Tree_RecPrint() {
    word vl[2] = {0,0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_RecPrint:
    r1 = *(vl[1].ptr);
    r2.n = 8;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[1];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto Tree_RecPrint_53;
    r6 = *(vl[1].ptr);
    r7.n = 4;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[1];
    (*(r9.f))();
    r10 = r0;
    r11 = *(vl[0].ptr);
    r12.n = 19;
    r13.ptr = r11.ptr + r12.n;
    r14 = *(r13.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = r10;
    (*(r14.f))();
    goto Tree_RecPrint_54;
Tree_RecPrint_53:
Tree_RecPrint_54:
    r17 = *(vl[1].ptr);
    r18.n = 5;
    r19.ptr = r17.ptr + r18.n;
    r20 = *(r19.ptr);
    param[next_param++] = vl[1];
    (*(r20.f))();
    r21 = r0;
    printf("%d\n", r21);
    r22 = *(vl[1].ptr);
    r23.n = 7;
    r24.ptr = r22.ptr + r23.n;
    r25 = *(r24.ptr);
    param[next_param++] = vl[1];
    (*(r25.f))();
    r26 = r0;
    if (r26.n == 0) goto Tree_RecPrint_55;
    r27 = *(vl[1].ptr);
    r28.n = 3;
    r29.ptr = r27.ptr + r28.n;
    r30 = *(r29.ptr);
    param[next_param++] = vl[1];
    (*(r30.f))();
    r31 = r0;
    r32 = *(vl[0].ptr);
    r33.n = 19;
    r34.ptr = r32.ptr + r33.n;
    r35 = *(r34.ptr);
    param[next_param++] = vl[0];
    param[next_param++] = r31;
    (*(r35.f))();
    goto Tree_RecPrint_56;
Tree_RecPrint_55:
Tree_RecPrint_56:
    r0.n = 1;
    return;
}

void Tree_accept() {
    word vl[2] = {0,0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Tree_accept:
    r1.n = 333;
    printf("%d\n", r1);
    r2 = *(vl[1].ptr);
    r3.n = 0;
    r4.ptr = r2.ptr + r3.n;
    r5 = *(r4.ptr);
    param[next_param++] = vl[1];
    param[next_param++] = vl[0];
    (*(r5.f))();
    r0.n = 0;
    return;
}

void Visitor_visit() {
    word vl[2] = {0,0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
Visitor_visit:
    r1 = *(vl[1].ptr);
    r2.n = 7;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[1];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto Visitor_visit_0;
    r6 = *(vl[1].ptr);
    r7.n = 3;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[1];
    (*(r9.f))();
    r10 = r0;
    r12.n = 2;
    r11.ptr = vl[0].ptr + r12.n;
    *(r11.ptr) = r10;
    r13.n = 2;
    r14.ptr = vl[0].ptr + r13.n;
    r15 = *(r14.ptr);
    r16 = *(r15.ptr);
    r17.n = 20;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = r15;
    param[next_param++] = vl[0];
    (*(r19.f))();
    goto Visitor_visit_1;
Visitor_visit_0:
Visitor_visit_1:
    r22 = *(vl[1].ptr);
    r23.n = 8;
    r24.ptr = r22.ptr + r23.n;
    r25 = *(r24.ptr);
    param[next_param++] = vl[1];
    (*(r25.f))();
    r26 = r0;
    if (r26.n == 0) goto Visitor_visit_2;
    r27 = *(vl[1].ptr);
    r28.n = 4;
    r29.ptr = r27.ptr + r28.n;
    r30 = *(r29.ptr);
    param[next_param++] = vl[1];
    (*(r30.f))();
    r31 = r0;
    r33.n = 1;
    r32.ptr = vl[0].ptr + r33.n;
    *(r32.ptr) = r31;
    r34.n = 1;
    r35.ptr = vl[0].ptr + r34.n;
    r36 = *(r35.ptr);
    r37 = *(r36.ptr);
    r38.n = 20;
    r39.ptr = r37.ptr + r38.n;
    r40 = *(r39.ptr);
    param[next_param++] = r36;
    param[next_param++] = vl[0];
    (*(r40.f))();
    goto Visitor_visit_3;
Visitor_visit_2:
Visitor_visit_3:
    r0.n = 0;
    return;
}

void MyVisitor_visit() {
    word vl[2] = {0,0};
    word r45 = {0};
    word r44 = {0};
    word r43 = {0};
    word r42 = {0};
    word r41 = {0};
    word r40 = {0};
    word r39 = {0};
    word r38 = {0};
    word r37 = {0};
    word r36 = {0};
    word r35 = {0};
    word r34 = {0};
    word r33 = {0};
    word r32 = {0};
    word r31 = {0};
    word r30 = {0};
    word r29 = {0};
    word r28 = {0};
    word r27 = {0};
    word r26 = {0};
    word r25 = {0};
    word r24 = {0};
    word r23 = {0};
    word r22 = {0};
    word r21 = {0};
    word r20 = {0};
    word r19 = {0};
    word r18 = {0};
    word r17 = {0};
    word r16 = {0};
    word r15 = {0};
    word r14 = {0};
    word r13 = {0};
    word r12 = {0};
    word r11 = {0};
    word r10 = {0};
    word r9 = {0};
    word r8 = {0};
    word r7 = {0};
    word r6 = {0};
    word r5 = {0};
    word r4 = {0};
    word r3 = {0};
    word r2 = {0};
    word r1 = {0};
    int p;
    for(p = 0; p <= 1 && p < 35; p++) {
        vl[p] = param[p];
    }
    next_param = 0;
MyVisitor_visit:
    r1 = *(vl[1].ptr);
    r2.n = 7;
    r3.ptr = r1.ptr + r2.n;
    r4 = *(r3.ptr);
    param[next_param++] = vl[1];
    (*(r4.f))();
    r5 = r0;
    if (r5.n == 0) goto MyVisitor_visit_0;
    r6 = *(vl[1].ptr);
    r7.n = 3;
    r8.ptr = r6.ptr + r7.n;
    r9 = *(r8.ptr);
    param[next_param++] = vl[1];
    (*(r9.f))();
    r10 = r0;
    r12.n = 2;
    r11.ptr = vl[0].ptr + r12.n;
    *(r11.ptr) = r10;
    r13.n = 2;
    r14.ptr = vl[0].ptr + r13.n;
    r15 = *(r14.ptr);
    r16 = *(r15.ptr);
    r17.n = 20;
    r18.ptr = r16.ptr + r17.n;
    r19 = *(r18.ptr);
    param[next_param++] = r15;
    param[next_param++] = vl[0];
    (*(r19.f))();
    goto MyVisitor_visit_1;
MyVisitor_visit_0:
MyVisitor_visit_1:
    r22 = *(vl[1].ptr);
    r23.n = 5;
    r24.ptr = r22.ptr + r23.n;
    r25 = *(r24.ptr);
    param[next_param++] = vl[1];
    (*(r25.f))();
    r26 = r0;
    printf("%d\n", r26);
    r27 = *(vl[1].ptr);
    r28.n = 8;
    r29.ptr = r27.ptr + r28.n;
    r30 = *(r29.ptr);
    param[next_param++] = vl[1];
    (*(r30.f))();
    r31 = r0;
    if (r31.n == 0) goto MyVisitor_visit_2;
    r32 = *(vl[1].ptr);
    r33.n = 4;
    r34.ptr = r32.ptr + r33.n;
    r35 = *(r34.ptr);
    param[next_param++] = vl[1];
    (*(r35.f))();
    r36 = r0;
    r38.n = 1;
    r37.ptr = vl[0].ptr + r38.n;
    *(r37.ptr) = r36;
    r39.n = 1;
    r40.ptr = vl[0].ptr + r39.n;
    r41 = *(r40.ptr);
    r42 = *(r41.ptr);
    r43.n = 20;
    r44.ptr = r42.ptr + r43.n;
    r45 = *(r44.ptr);
    param[next_param++] = r41;
    param[next_param++] = vl[0];
    (*(r45.f))();
    goto MyVisitor_visit_3;
MyVisitor_visit_2:
MyVisitor_visit_3:
    r0.n = 0;
    return;
}

