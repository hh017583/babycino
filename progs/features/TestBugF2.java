class TestBugF2 {

    // Evaluate y even when x is true

    public static void main(String[] a) {
	if (new Test().f()) {} else {}
    }
}

class Test {

    public boolean f() {
    int n;
    int c;
    c = 0;
    n = 1;
	while (c < 5 || this.y(n)) {   // assume C < 5 for x
        System.out.println(n + 1);
        c = c + 1;
    }
	return false;
    }



    public boolean y(int n) {
        System.out.println(0);
        return false;
    }

}

// if it ouput 0 before 2, means y does evaluated even when x is true.
// if it output 2 five times, means x is true, and the expersion return true
// it should print out 0 after five 2 printed
// because x returned false;