class TestBugE1{

    // bug::Allow comparison of boolean

    public static void main(String[] arg){
            System.out.println(new Test().E1());
    }
}


class Test {

    public int E1(){
        int a;
        int b;
        int res;

        a = 2;
        b = 3;
        res = 0;
        while ((b > a) > (a>b)){
            res = 1;
        }
        return res;

    }
}
//output:
//Expected int as 1st argument to >; actual type: boolean
//Context: (b>a)>(a>b)
//Expected int as 2nd argument to >; actual type: boolean
//Context: (b>a)>(a>b)
//Exiting due to earlier error.
